#!/bin/bash

sudo dpkg --force-overwrite -i openjdk-8-jdk-headless_*.deb openjdk-8-jdk_*.deb openjdk-8-jre-headless_*.deb openjdk-8-jre-zero_*.deb openjdk-8-jre_*.deb
