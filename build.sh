#!/bin/bash

export DEB_BUILD_OPTIONS=nocheck

sudo apt source openjdk-8-jre && \
sudo apt install build-essential && \
sudo apt build-dep openjdk-8-jre && \
sudo chmod 755 openjdk-8-?????-* &&
cd openjdk-8-?????-* && \
(sudo patch --forward -p1 <../rules.patch; exit 0) && \
sudo rm -f config.log && \
dpkg-buildpackage -b -uc -us && \
sudo chown -R jh.jh .
